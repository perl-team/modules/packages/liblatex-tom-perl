liblatex-tom-perl (1.06-1) unstable; urgency=medium

  * Import upstream version 1.06.

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Apr 2023 17:26:40 +0200

liblatex-tom-perl (1.05-1) unstable; urgency=medium

  * Import upstream version 1.05.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Feb 2023 21:00:22 +0100

liblatex-tom-perl (1.04-1) unstable; urgency=medium

  * Import upstream version 1.04.
  * Remove spelling.patch, applied upstream.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Feb 2023 22:14:37 +0100

liblatex-tom-perl (1.03-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + liblatex-tom-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:44:09 +0100

liblatex-tom-perl (1.03-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 15:07:42 +0100

liblatex-tom-perl (1.03-1) unstable; urgency=medium

  * Take over for the Debian Perl Group; Closes: #867754 -- RFA/ITA
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza). Changed: Homepage field changed to
    metacpan.org URL; Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Patrick Winnertz
    <winnie@debian.org>).
  * debian/watch: use metacpan-based URL.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.0.
  * Drop version from perl build dependency.
  * Switch to source format "3.0 (quilt)".
  * Bump debhelper compatibility level to 9.
  * Use dh(1).
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Add /me to Uploaders.
  * Update short and long description.
  * Cleanup build dependencies.

  * New upstream release.
  * Add a spelling patch.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Sep 2017 20:14:12 +0200

liblatex-tom-perl (1.00-1) unstable; urgency=low

  * New upstream release (Closes: #531336)
  * Added watchfile to see when newer version are released.
  * Added Hompage: to control
  * Bumped standards-version to 3.8.3, no further changes needed

 -- Patrick Winnertz <winnie@debian.org>  Mon, 25 Jan 2010 22:39:34 +0100

liblatex-tom-perl (0.8-1) unstable; urgency=low

  * New upstream release
  * Changed maintainer address
  * Bumped Standard-Versions number
  * Remove .packlist from package (added find ... | xargs rm -f statement to
    debian/rules

 -- Patrick Winnertz <winnie@debian.org>  Thu, 06 Dec 2007 19:31:01 +0100

liblatex-tom-perl (0.6-1) unstable; urgency=low

  * Initial release (Closes: #417031)

 -- Patrick Winnertz <patrick.winnertz@skolelinux.org>  Fri, 30 Mar 2007 17:37:06 +0200
